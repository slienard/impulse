Impulse v1.5, by Duranik (1996,1997)

Patch <VERSION>, by SLiX (2022)

How to use:
* Get Impulse v1.5 from http://www.duranik.com/falcon.html
* Extract files.
* Add IMPULSE.TOS from this archive.
* Run IMPULSE.TOS instead of IMPULSE.PRG.
