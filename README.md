# Impulse

[Impulse](http://www.duranik.com/falcon.html) is a classic breakout clone for the [Atari Falcon030](https://wikipedia.org/wiki/Atari_Falcon) released as freeware by [Duranik](http://www.duranik.com) in 1996.

![snapshot1](img/snap1.png) ![snapshot2](img/snap2.png)

This project provides a patched version with the following changes:
* Mouse speed selection screen with these settings:
  * Original mouse acceleration (with fix, was moving faster to the left)
  * Constant Nx speed (N = 1, 2 or 3)
